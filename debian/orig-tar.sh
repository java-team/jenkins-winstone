#!/bin/sh -e

VERSION=$2
TAR=../jenkins-winstone_$VERSION.orig.tar.xz
DIR=jenkins-winstone-$VERSION
mkdir -p $DIR

# Expand the upstream tarball
tar -xzf $3 -C $DIR --strip-components=1
rm $3

# Repack excluding stuff we don't need
XZ_OPT=--best tar -c -J -v -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude 'CVS' \
    --exclude '.svn' \
    $DIR
rm -rf $DIR

